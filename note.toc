\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {UKenglish}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Description of the ASIC Prototypes}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}LAUROC0}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}HLC1}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Test Bench}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Front-End Test Board}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Toy Calorimeter Board}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Front-End Mezzanine}{6}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}DAQ Board}{6}{subsection.3.4}
\contentsline {section}{\numberline {4}Characterization of the Prototypes}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}LAUROC0}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}input impedance tuning}{7}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Linearity measurement}{7}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Charachterization of the Internal Discriminator}{7}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Noise Measurement}{8}{subsubsection.4.1.4}
\contentsline {paragraph}{\nonumberline Setup}{8}{section*.6}
\contentsline {paragraph}{\nonumberline Methods}{8}{section*.7}
\contentsline {paragraph}{\nonumberline Results}{8}{section*.8}
\contentsline {subsubsection}{\numberline {4.1.5}Cross-talk}{9}{subsubsection.4.1.5}
\contentsline {paragraph}{\nonumberline Setup}{9}{section*.10}
\contentsline {paragraph}{\nonumberline Method}{9}{section*.11}
\contentsline {paragraph}{\nonumberline Results}{9}{section*.12}
\contentsline {subsection}{\numberline {4.2}HLC1}{9}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}input impedance tuning}{9}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Linearity measurement}{10}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Noise Measurement}{10}{subsubsection.4.2.3}
\contentsline {paragraph}{\nonumberline Setup}{10}{section*.14}
\contentsline {paragraph}{\nonumberline Method}{10}{section*.15}
\contentsline {paragraph}{\nonumberline Results}{10}{section*.16}
\contentsline {subsubsection}{\numberline {4.2.4}Summing Outputs}{11}{subsubsection.4.2.4}
\contentsline {paragraph}{\nonumberline Setup}{11}{section*.19}
\contentsline {paragraph}{\nonumberline Methods}{11}{section*.20}
\contentsline {paragraph}{\nonumberline Results}{11}{section*.21}
\contentsline {subsubsection}{\numberline {4.2.5}Cross-talk}{12}{subsubsection.4.2.5}
\contentsline {paragraph}{\nonumberline Setup}{12}{section*.23}
\contentsline {paragraph}{\nonumberline Methods}{12}{section*.24}
\contentsline {paragraph}{\nonumberline Results}{12}{section*.25}
\contentsline {subsection}{\numberline {4.3}Summary and Comparison}{13}{subsection.4.3}
\contentsline {section}{\numberline {5}Conclusion}{13}{section.5}
\contentsline {part}{Appendices}{15}{part*.29}
